'use strict';
const { series, src, dest } = require('gulp');
var gulp = require('gulp'),
    sass = require('gulp-sass'),
    browserSync = require('browser-sync'),
    del = require('del'),
    imagemin = require('gulp-imagemin'),
    uglify = require('gulp-uglify'),
    usemin = require('gulp-usemin'),
    rev = require('gulp-rev'),
    cleanCss = require('gulp-clean-css'),
    flatmap = require('gulp-flatmap'),
    hmtlmin = require('gulp-htmlmin');
const htmlmin = require('gulp-htmlmin');

sass.compiler = require('node-sass');

gulp.task('sass', function() {
    return src('./css/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(dest('/.css'));
});

gulp.task('sass:watch', function() {
    gulp.watch('./css/*.scss', ['sass']);
});

gulp.task('browser-sync', function() {
    var files = ['./*.html', './css/*.css', './img/*.{png. jpg, gif}', './js/*.js']
    browserSync.init(files, {
        server: {
            baseDir: './'
        }
    });
});

gulp.task('default', gulp.series('browser-sync', function() {
    gulp.task('sass:watch');
}));

gulp.task('clean', function() {
    return del(['dist']);
});

gulp.task('copyfonts', function() {
    return src('./node_modules/open_iconic/font/fonts/*.{ttf, woff, eof, svg, eot, otf}')
        .pipe(dest('./dist/fonts'));
});

gulp.task('imagemin', function() {
    return src('./images/*.{png, jpg, jpeg, gif}')
        .pipe(imagemin({ optimizationLevel: 3, progressive: true, interleced: true }))
        .pipe(dest('dist/images'));
});

gulp.task('usemin', function() {
    return src('./*.html')
        .pipe(flatmap(function(stream, file) {
            return stream
                .pipe(usemin({
                    css: [rev()],
                    html: [function() { return htmlmin({ collapseWhitespace: true }) }],
                    js: [uglify(), rev()],
                    inlinejs: [uglify()],
                    inlinecss: [cleanCss(), 'concat']
                }));
        }))
        .pipe(dest('dist/.'));
});


/*gulp.task('build', gulp.series('clean', function(done) {
    gulp.start('copyfonts', 'imagemin', 'usemin');
}));*/

exports.build = series('clean', 'copyfonts', 'imagemin', 'usemin');