$(function() {
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $('.carousel').carousel({
        interval: 1500
    });
    $('#saludo').on('show.bs.modal', function(e) {
        $('#inscribirme').removeClass('btn-info');
        $('#inscribirme').addClass('btn-warning');
        $('#inscribirme').prop('disabled', true);
    });
    $('#saludo').on('hidden.bs.modal', function(e) {
        $('#inscribirme').prop('disabled', false);
        $('#inscribirme').removeClass('btn-warning');
        $('#inscribirme').addClass('btn-info');
    });
});